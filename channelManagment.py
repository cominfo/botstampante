import requests, datetime

channelId = '-1001320286257'
levelGood = 25 # livello sopra il quale lo stato è buono
levelBad  = 0#10 # livello sotto il quale (incluso) lo stato è cattivo

def call(method,payload):
	res = requests.post('https://api.telegram.org/bot<BOT_TOKEN>/'+method,params=payload)
	if res.status_code == 200:
		return res.json()
	else:
		print('Error in channelManagment::call\n+-Status not 200',flush=True)
		print(res.text,flush=True)#DEBUG
		return {'result':{'message_id':None}}

notification = False
mainMessageId = None
def setMainMessage(text):
	global mainMessageId, notification
	if mainMessageId is None:
		mainMessageId = call('sendMessage',{'chat_id':channelId,'parse_mode':'Markdown','text':text,'disable_notification':'false' if notification else 'true'})['result']['message_id']
	else:
		mainMessageId = call('editMessageText',{'chat_id':channelId,'message_id':mainMessageId,'parse_mode':'Markdown','text':text})['result']['message_id']
	notification = False

def alert(text):
	global mainMessageId, notification
	call('sendMessage',{'chat_id':channelId,'text':text,'disable_notification':'false'})
	notification = True
	mainMessageId = None

notified = [[False,False,False,False],[False,False,False,False,False]]
nomi = [['Toner','Nero','Ciano','Magenta','Giallo'],['Vassoio',1,2,3]]
def updateStatus(info):
	status=[]
	for i in [0,1]:
		a=[]
		for j,x in enumerate(info[i]):
			a.append('✅' if x>levelGood else '⚠' if x>levelBad else '❌')
			if x>levelBad:
				notified[i][j]=False
			elif not notified[i][j]:
				notified[i][j]=True
				alert('⚠ ATTENZIONE !!! ⚠\n%s %s esaurito!' % (nomi[i][0],nomi[i][j+1]))
		status.append(a)
	setMainMessage('''```
==== Inchiostro =====
Nero:         %3u%% %s
Ciano:        %3u%% %s
Magenta:      %3u%% %s
Giallo:       %3u%% %s
======= Carta =======
Vassoio 1:    %3u%% %s
Vassoio 2:    %3u%% %s
Vassoio 3:    %3u%% %s
=====================
Ultimo aggiornamento:
%s
\
```''' % (info[0][0],status[0][0],info[0][1],status[0][1],info[0][2],status[0][2],info[0][3],status[0][3],
			 info[1][0],status[1][0],info[1][1],status[1][1],info[1][2],status[1][2],
			 datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")))
