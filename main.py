#!/usr/bin/python3

import get_info, threading, sys
import channelManagment as cM

ore = 1 # ogni quante ore aggiornare

#sys.stdout = sys.stderr

def call_repeatedly(interval, func, *args):
	func(*args)
	stopped = threading.Event()
	def loop():
		while not stopped.wait(interval): # the first call is in `interval` secs
			func(*args)
	threading.Thread(target=loop).start()
	return stopped.set

def updater():
	try:
		info = get_info.get_info()
		if info is not None:
			cM.updateStatus(info)
	except Exception as e:
		print(e)

call_repeatedly(ore*60*60,updater)