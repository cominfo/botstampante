# Bot Stampante

Questo bot legge ogni ora lo stato di carta e inchiostro della stampante in aula info e manda i messaggi sul relativo canale Telegram.

## Come installarlo

1. Scarica il programma compilato da GitLab: https://gitlab.com/cominfo/botstampante/-/jobs/artifacts/master/download?job=build-windows
2. Copiare la cartella `main` in `C:\Program Files (x86)\Intel\main` sul server (il pc collegato direttamente alla stampante)
3. Mettere in avvio automatico `C:\Program Files (x86)\Intel\main\inisrte.vbs`:
   1. Premi Super+R (Super è il tasto col logo di Winzoz)
   2. Digita `shell:startup` e premi Ok 
   3. Copia nella cartella che si aprirà il collegamento a `inisrte.vbs`
